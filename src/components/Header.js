import React, { Component } from 'react';
import PropTypes from 'prop-types';

const Header = (props) => {
    return (
        <div>
            <header className="navbar-top">
                <div>{props.title}</div>
            </header>
        </div>
    );
}


export default Header;