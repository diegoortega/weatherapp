import React, { Component } from 'react';
import logo from './logo.svg';
import './css/App.css';
import Header from './components/Header';
import Formulario from './components/Formulario';
import PropTypes from 'prop-types';
import WeatherIcons from 'react-weathericons';

class App extends Component {

 
  verApi = (respuesta) => {
    const {ciudad, pais} = respuesta;
    console.log(ciudad);
    console.log(pais);

    const apikey = '0f2845168007b0bbb449a3c78f11a2ba';
    let url = `api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${apikey}`;
  
    console.log(url);
  }

  datosConsulta = (respuesta) => {
    this.verApi(respuesta);
  } 
  render() {
    return (
      <div className="App">
        <Header title ="My fucking app"/>
        <WeatherIcons name="tsunami" size="3x"/ >
        <Formulario eldato ={this.datosConsulta}/>
      </div>
    );
  }
}

export default App;
